import React, { Component } from 'react';

class Toggle extends React.Component{
    constructor(props){
        super(props);
        this.state = {value: true};
        this.toggleClick = this.toggleClick.bind(this);
    }

    toggleClick(){
        /*var value = this.state.value;
        this.setState({
            value: !value
        });*/

        this.setState( prevState =>({
            value: !prevState.value
        }));
    }
    render () {
        return (<button onClick={this.toggleClick}>{this.state.value ? "TRUE": "FALSE"}</button>);
    }
}
 export default Toggle;
