import React, { Component } from 'react';

class Input extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            value: 'Значение'
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e){
        console.log("change");
        this.setState ({value: e.target.value});
    }
    render () {
        return (
            <div>
                <input onChange={this.handleChange} type="text" value={this.state.value}/>
                <div>
                    <input type="text" defaultValue="defaultValue"/>
                </div>
                <div>
                    <input type="checkbox"/>
                    <input type="checkbox" defaultChecked/>
                    <input type="checkbox"/>
                    <input type="checkbox"/>
                    <input type="checkbox"/>
                </div>
                <div>
                    <input name="group" type="radio"/>
                    <input name="group" type="radio"/>
                    <input name="group" type="radio" defaultChecked/>
                    <input name="group" type="radio"/>
                </div>
                <div>
                    <select defaultValue="Чебурашка" name="hero">
                        <option>Выберите героя</option>
                        <option value="Чебурашка">Чебурашка</option>
                        <option value="Крокодил Гена">Крокодил Гена</option>
                        <option value="Шапокляк">Шапокляк</option>
                        <option value="Крыса Лариса">Крыса Лариса</option>
                    </select>
                </div>
                <div>
                    <textarea defaultValue="textarea"></textarea>
                </div>
            </div>
        );
    }
}
 export default Input;
