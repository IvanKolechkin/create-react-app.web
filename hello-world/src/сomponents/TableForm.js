import React, { Component } from 'react';
import TableBody from './TableBody';

class TableForm extends React.Component{
    constructor(props){
        super(props);
        this.state = ({value:""});
        this.handlerChange = this.handlerChange.bind(this);
    }
    handlerChange(e){
        this.setState({
            value: e.target.value
        });
    }
    render () {
        return (
            <div>
                <div>
                    <input onChange={this.handlerChange} type="text" value={this.state.value}/>
                    <TableBody data={this.props.data} search={this.state.value}/>
                </div>
            </div>
        );
    }
}
 export default TableForm;
