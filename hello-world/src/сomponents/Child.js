import React, { Component } from 'react';

class Child extends React.Component{
    render () {
        return (
            <div>
                {this.props.children}
                <br/>
                {this.props.form}
            </div>
        );
    }
}
 export default Child;
