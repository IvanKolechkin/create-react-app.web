import React, { Component } from 'react';
import TableRow from './TableRow';

class TableBody extends React.Component{
    render () {
        var rows = [],
            search = this.props.search,
            index = 0;

        this.props.data.forEach((product) => {
            var name = product.name;

            if (!(name.indexOf(search) === -1)){
                index++;
                rows.push(<TableRow key={name} name={name} price={product.price} />);
            }

        });
        return (
            <div>
                {search != "" ? <p>Поиск: {search}</p> : ""}
                <div>
                    {index == 0 ? <p>Совпадений не найдено</p> : <p>Количество: {index}</p>}
                    {rows}
                </div>
            </div>
        );
    }
}
export default TableBody;
