import React, { Component } from 'react';
import TableForm from './TableForm';

var PRODUCTS = [
  {price: '$49.99', name: 'Football'},
  {price: '$9.99', name: 'Baseball'},
  {price: '$29.99', name: 'Basketball'},
  {price: '$99.99', name: 'iPod Touch'},
  {price: '$399.99', name: 'iPhone 5'},
  {price: '$199.99', name: 'Nexus 7'}
];

class Table extends React.Component{
    render () {
        return (
            <div>
                <TableForm data={PRODUCTS}/>
            </div>
        );
    }
}
 export default Table;
