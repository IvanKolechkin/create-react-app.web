import React, { Component } from 'react';

class Clock extends React.Component {
    //Объявляю состояние
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    //Вставлен в DOM
    componentDidMount() {
        console.log("sss");
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}


export default Clock;
