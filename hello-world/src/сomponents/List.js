import React, { Component } from 'react';

class ListItem extends React.Component {
    render(){
        var val = this.props.data;
        return(
            <li>{val}</li>
        );
    }
}

class List extends React.Component{
    render(){
        var numbers = this.props.data;
        const listItems = numbers.map((number,index) =>
          <ListItem key={index} data={number}/>
        );
        return (
            <div>
                <ul>{listItems}</ul>
            </div>
        );
    }
}

export default List;
