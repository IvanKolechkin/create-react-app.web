import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
/*Подключаю компоненты*/
import Clock from './сomponents/Clock';
import Toggle from './сomponents/Toggle';
import Page from './сomponents/Page';
import List from './сomponents/List';
import Form from './сomponents/Form';
import Input from './сomponents/Input';
import Child from './сomponents/Child';

import Table from './сomponents/Table';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
        To get started, ччччч <code>src/App.js</code> and save to reload.
        </p>
        <div>
            <Clock />
        </div>
        <div>
            <Toggle />
        </div>
        <div>
            <Page />
        </div>
        <div>
            <List data={[21,332,3,4324,35]}/>
        </div>
        <div>
            <Form />
        </div>
        <br/>
        <br/>
        <br/>
        <div>
            <Child form={<div>Вставляю form через props.form <br/><Form /></div>}>
                <Form/>
            </Child>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <Table />
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
      </div>
    );
  }
}

export default App;
